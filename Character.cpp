/**
 * @file Character.cpp
 * @author algarcia
 *
 * @date 17 de marzo de 2021, 9:17
 *
 * @brief Implementation of the methods of the class Character
 */

#include "Character.h"

/**
 * Parameterized constructor
 * @param nRole Role of the new character (wizard, warrior, elf...)
 * @param nLifeP Life points of the character
 * @pre No check is done on the parameters
 * @post The new character will store the values passed as parameters
 */
Character::Character ( std::string nRole, int nLifeP ): _role ( nRole )
                                                      , _lifePoints ( nLifeP )
{ }


/**
 * Copy constructor
 * @param orig Character from which the data is copied
 * @post The new character will store values exactly equal to the ones of the
 *       original
 */
Character::Character ( const Character& orig ): _role ( orig._role )
                                              , _lifePoints ( orig._lifePoints )
{ }


/**
 * Destructor
 */
Character::~Character ( ) { }


/**
 * Changes the life points of the character
 * @param nLifeP New life points value
 * @pre No check is done on the parameter value
 * @post The life points of the character are changed
 */
void Character::setLifePoints ( int nLifeP )
{
   this->_lifePoints = nLifeP;
}


/**
 * Queries the character for its life points
 * @return The life points of the character
 */
int Character::getLifePoints ( ) const
{
   return _lifePoints;
}


/**
 * Changes the role of the character
 * @param nRole Describes the new role of the character (wizard, warrior, elf...)
 * @pre No check is performed on the parameter value
 * @post The role of the character is changed
 */
//void Character::setRole ( std::string nRole )
//{
//   this->_role = nRole;
//}


/**
 * Queries the character for its role
 * @return The role of the character
 */
std::string Character::getRole ( ) const
{
   return _role;
}


/**
 * Assignment operator
 * @param other Character from which the values are assigned
 * @post The attributes of the character will store values exactly equal to the
 *       ones of the original
 * @return A reference to the character, in order to allow chained assignments
 */
Character& Character::operator= ( Character& other )
{
   if ( this != &other )
   {
      _role = other._role;
      _lifePoints = other._lifePoints;
   }

   return *this;
}


bool Character::operator != ( Character& other )
{
   bool toRet = true;

   if ( this != &other )
   {
      if ( _lifePoints != other._lifePoints )
      {
         toRet = false;
      }

      if ( _role != other._role )
      {
         toRet = false;
      }
   }

   return toRet;
}
