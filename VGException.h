/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   VGException.h
 * Author: algarcia
 *
 * Created on 28 de abril de 2021, 9:02
 */

#ifndef VGEXCEPTION_H
#define VGEXCEPTION_H

#include <stdexcept>
#include <string>


class VGException: public std::runtime_error
{
   private:
  
   public:
      VGException ( std::string msg );
      VGException ( const VGException& orig );
      virtual ~VGException ( ) noexcept = default;
} ;

#endif /* VGEXCEPTION_H */

