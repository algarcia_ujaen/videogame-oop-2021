/** 
 * @file ControllableCharacter.h
 * @author algarcia
 *
 * @date 24 de marzo de 2021, 9:11
 * 
 * @brief Declaration of the class ControllableCharacter
 */

#ifndef CONTROLLABLECHARACTER_H
#define CONTROLLABLECHARACTER_H

#include "Character.h"

/**
 * The objects of this class represent video game characters that can be
 * controlled by the players
 */
class ControllableCharacter: public Character
{
   private:
      std::string _name = "";   ///< Name of the character

   public:
      ControllableCharacter ( ) = default;
      ControllableCharacter ( std::string nName, std::string nRole );
      ControllableCharacter ( const ControllableCharacter& orig );
      virtual ~ControllableCharacter ( );
      std::string getName ();
      ControllableCharacter& setName ( std::string& nName);
      ControllableCharacter& operator= ( ControllableCharacter& other );
      virtual void setRole ( std::string nRole ) override;
};

#endif /* CONTROLLABLECHARACTER_H */

