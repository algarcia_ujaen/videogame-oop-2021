/* 
 * File:   Container.h
 * Author: algarcia
 *
 * Created on 14 de abril de 2021, 9:09
 */

#ifndef CONTAINER_H
#define CONTAINER_H

#include <stdexcept>


template<typename T>
class Container
{
   public:
      static const int MAX_CONTAINER_SIZE = 10;

   private:
      T _elements[MAX_CONTAINER_SIZE];   ///< Contained elements
      int _numElements = 0;   ///< Number of valid elements in the container
      T _voidValue = T();   ///< Value for the "empty" positions
      
      void defrag ( int pos );

   public:
      Container ();
      Container ( T newVoid );
      Container ( const Container& orig );
      virtual ~Container ();
      T getPos ( int which );
      void addElement ( const T& newOne );
      int getNumElements ();
      T takeOut ( int which );
      bool operator== ( const Container& other );
      bool operator!= ( const Container& other );
      T getVoidValue ();
};

/**
 * Moves the elements to optimize the storage
 * @param pos Position that has a "void" value
 */
template<typename T>
void Container<T>::defrag ( int pos )
{
   for ( int i = pos; i < _numElements; i++ )
   {
      _elements[i] = _elements[i+1];
   }
   
   _elements[_numElements] = _voidValue;
}

/**
 * Default constructor
 */
template<typename T>
Container<T>::Container ()
{
   for ( int i = 0; i < MAX_CONTAINER_SIZE; i++ )
   {
      _elements[i] = _voidValue;
   }
}

/**
 * Parameterized constructor
 * @param newVoid Value to be assigned to those positions that are "empty"
 */
template<typename T>
Container<T>::Container ( T newVoid ): _voidValue ( newVoid )
{
   for ( int i = 0; i < MAX_CONTAINER_SIZE; i++ )
   {
      _elements[i] = _voidValue;
   }
}

/**
 * Copy constructor
 * @param orig Container whose values are copied
 */
template<typename T>
Container<T>::Container ( const Container& orig ): _numElements ( orig._numElements )
                                                 , _voidValue ( orig._voidValue )
{
   for ( int i = 0; i < MAX_CONTAINER_SIZE; i++ )
   {
      _elements [i] = orig._elements [i];
   }
}

/**
 * Destructor
 * @post Assigns the "void" value to all the elements in the container
 */
template<typename T>
Container<T>::~Container ()
{
   for ( int i = 0; i < MAX_CONTAINER_SIZE; i++ )
   {
      _elements[i] = _voidValue;
   }
}

/**
 * Access one element of the container
 * @param which Position of the element (valid values: from 1 to the number of
 *        stored elements )
 * @return The queried element
 * @throw std::length_error If the value of the position is not correct
 */
template<typename T>
T Container<T>::getPos ( int which )
{
   if ( ( which < 1 ) || ( which > _numElements ) )
   {
      throw std::length_error ( "[Container::getPos]: invalid index" );
   }

   return _elements[which-1];
}

/**
 * Adds a new element to the container
 * @param newOne Value to be added
 * @throw std::length_error If there is no more room in the container
 */
template<typename T>
void Container<T>::addElement ( const T& newOne )
{
   if ( _numElements == MAX_CONTAINER_SIZE )
   {
      throw std::length_error ( "[Container::addElement]: no room for more elements!" );
   }

   _elements[_numElements] = newOne;
   _numElements++;
}

/**
 * Asks the container for the number of elements actually stored
 * @return The number of elements in the container
 */
template<typename T>
int Container<T>::getNumElements ()
{
   return _numElements;
}

/**
 * Takes out one element from the container
 * @param which Position of the element to be removed (valid values: from 1 to
 *        the number of stored elements )
 * @return The element that has been taken out
 */
template<typename T>
T Container<T>::takeOut ( int which )
{
   if ( ( which < 1 ) || ( which > _numElements ) )
   {
      throw std::length_error ( "[Container::deletePos]: index out of bounds!" );
   }
   
   T toRet = _elements[which - 1];
   _numElements--;
   
   defrag ( which - 1 );
   
   return toRet;
}

/**
 * "Equal to" operator
 * @param other Container to compare with
 * @retval true If the containers are equal
 * @retval false If the containers are not equal
 */
template<typename T>
bool Container<T>::operator== ( const Container& other )
{
   bool toRet = true;
   
   if ( this != &other )
   {
      if ( _numElements != other._numElements )
      {
         toRet = false;
      }
      else
      {
         for ( int i = 0; i < _numElements; i++ )
         {
            if ( _elements[i] != other._elements[i] )
            {
               toRet = false;
            }
         }
      }
   }

   return toRet;
}

/**
 * "Different from" operator
 * @param other Container to compare with
 * @retval true If the containers are different
 * @retval false If the containers are equal
 */
template<typename T>
bool Container<T>::operator!= ( const Container& other )
{
   return !operator == ( other );
}

/**
 * Asks the container for the value used for "empty" locations
 * @return The value used for "empty" locations
 */
template<typename T>
T Container<T>::getVoidValue ()
{
   return _voidValue;
}

#endif /* CONTAINER_H */

