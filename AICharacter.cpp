/**
 * @file AICharacter.cpp
 * @author algarcia
 *
 * @date 24 de marzo de 2021, 9:11
 *
 * @brief Implementation of the methods of the class AICharacter
 */

#include "AICharacter.h"

/**
 * @brief Parameterized constructor
 * @param role Role of the new character (wizard, warrior, elf...)
 * @param lt Life time of the new character
 * @pre No check is done on the parameters
 * @post A new character is created. The parameters are assigned to its
 *       attributes
 */
AICharacter::AICharacter ( std::string role, int lt ): Character ( role, 0 )
                                                     , _lifeTime (lt)
{ }


/**
 * @brief Copy constructor
 * @param orig Character to be copied
 * @post A new character is copied. Its attributes are exact copies of the
 *       attributes of the original ones
 */
AICharacter::AICharacter ( const AICharacter& orig ): Character ( orig )
                                                    , _lifeTime ( orig._lifeTime )
{ }


/**
 * @brief Destructor
 */
AICharacter::~AICharacter ( ) { }


/**
 * @brief Observer for the character life time
 * @return The life time of the character
 */
int AICharacter::getLifeTime () const
{
   return _lifeTime;
}


/**
 * @brief Modifier of the character life time
 * @param lt New life time value
 * @pre No check is performed on the parameter value
 * @return A reference to the character, to allow chained method calls
 */
AICharacter& AICharacter::setLifeTime ( int lt )
{
   _lifeTime = lt;
   return *this;
}


/**
 * @brief Assignment operator
 * @param other Character from which the attributes are copied
 * @return A reference to the character, to allow chained assignments
 * @post The character attributes will contain exact copies of the ones from the
 *       other character
 */
AICharacter& AICharacter::operator= ( AICharacter& other )
{
   if ( this != &other )
   {
      Character::operator= ( other );
      _lifeTime = other._lifeTime;
   }

   return *this;
}

void AICharacter::setRole ( std::string nRole )
{
   _role = nRole + " - AI";
}