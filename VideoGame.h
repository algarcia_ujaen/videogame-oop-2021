/** 
 * @file VideoGame.h
 * @author algarcia
 *
 * @date 10 de febrero de 2021, 9:23
 * 
 * @brief Declaration of the VideoGame class
 */

#ifndef VIDEOGAME_H
#define VIDEOGAME_H

#include <string>
#include "Character.h"
#include "ControllableCharacter.h"
#include "AICharacter.h"
#include "Container.h"

#define MAX_CHARACTERS 100

/**
 * Class for videogames.
 * 
 * The objects of this class represent videogames with their related information
 */
class VideoGame
{
   private:
      std::string _title = "";   ///< The title of the videogame
      std::string _genre = "";   ///< The genre of the videogame
      int _launchDate = 0;       ///< The launch date. Format: YYYYMMDD
      std::string* _version = nullptr;   ///< Version of the videogame
//      Character* _characters[MAX_CHARACTERS];   ///< Pointers to characters
//      int _nCharacters = 0;
      Container<Character*> _characters;

   public:
      VideoGame ( );
      VideoGame ( std::string nTitle, std::string nGenre, std::string version );
      VideoGame ( VideoGame& orig );
      virtual ~VideoGame ( );
      void setLaunchDate ( int launchDate );
      int getLaunchDate ( ) const;
      void setGenre ( std::string genre );
      std::string getGenre ( ) const;
      void setTitle ( std::string title );
      std::string getTitle ( ) const;
      bool operator== ( const VideoGame& other );
      VideoGame& operator= ( VideoGame& other );
//      VideoGame& addCharacter ( Character& nC );
      VideoGame& addCharacter ( AICharacter& nC );
      VideoGame& addCharacter ( ControllableCharacter& nC );
      Character* getCharacter ( int which );
      void deleteCharacter ( int which );
      int getNumberOfCharacters ();
      void changeCharacterRoles ();
};

#endif /* VIDEOGAME_H */

