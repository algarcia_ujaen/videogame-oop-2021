/** 
 * @file Player.h
 * @author algarcia
 *
 * @date 10 de marzo de 2021, 9:25
 * 
 * @brief Declaration of the class Player
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "VideoGame.h"

/**
 * The objects of this class represent video game players
 */
class Player
{
   private:
      std::string _realName = "";   ///< Real name of the player
      std::string _alias = "";      ///< Nickname of the player
      int _score = 0;               ///< Scored points
      VideoGame* _game = nullptr;   ///< Pointer to the video game they are playing
  
   public:
      Player ( ) = default;
      Player ( const Player& orig );
      virtual ~Player ( );
      void setScore ( int newScore );
      int getScore ( ) const;
      void setAlias ( std::string newAlias );
      std::string getAlias ( ) const;
      void setRealName ( std::string newRealName );
      std::string getRealName ( ) const;
      void setGame ( VideoGame* newGame );
      void setGame ( VideoGame& newGame );
      VideoGame* getGame ( ) const;
      Player& operator = ( const Player& right );
};

#endif /* PLAYER_H */

