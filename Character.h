/** 
 * @file Character.h
 * @author algarcia
 *
 * @date 17 de marzo de 2021, 9:17
 * 
 * @brief Declaration of the class Character
 */

#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>

/**
 * Objects of this class represent characters of the video game
 */
class Character
{
   private:
      int _lifePoints = 0;   ///< Life points of the character

   protected:
      std::string _role = "";   ///< Describes the role of the character (wizard, warrior, elf...)

   public:
      Character ( ) = default;
      Character ( std::string nRole, int nLifeP );
      Character ( const Character& orig );
      virtual ~Character ( );
      void setLifePoints ( int nLifeP );
      int getLifePoints ( ) const;
      virtual void setRole ( std::string nRole ) = 0;
      std::string getRole ( ) const;
      Character& operator= ( Character& other );
      bool operator!= ( Character& other );
};

#endif /* CHARACTER_H */

