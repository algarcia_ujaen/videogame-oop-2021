/** 
 * @file AICharacter.h
 * @author algarcia
 *
 * @date 24 de marzo de 2021, 9:11
 * 
 * @brief Declaration of the class AICharacter
 */

#ifndef AICHARACTER_H
#define AICHARACTER_H

#include "Character.h"

/**
 * Objects of this class represent characters controlled by the videogame IA
 * engine
 */
class AICharacter: public Character
{
   private:
      int _lifeTime = 0;   ///< Life time of the character

   public:
      AICharacter ( ) = default;
      AICharacter ( std::string role, int lt );
      AICharacter ( const AICharacter& orig );
      virtual ~AICharacter ( );
      int getLifeTime () const;
      AICharacter& setLifeTime ( int lt );
      AICharacter& operator= ( AICharacter& other );
      virtual void setRole ( std::string nRole ) override;
};

#endif /* AICHARACTER_H */

