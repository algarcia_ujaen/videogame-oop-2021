/**
 * @file ControllableCharacter.cpp
 * @author algarcia
 *
 * @date 24 de marzo de 2021, 9:11
 *
 * @brief Implementation of the methods of the class ControllableCharacter
 */

#include "ControllableCharacter.h"

/**
 * Parameterized constructor
 * @param nName Name of the new character
 * @param nRole Role of the new character (wizard, warrior, elf...)
 * @post The new character has the values given by the attributes, and 0 life
 *       points
 */
ControllableCharacter::ControllableCharacter ( std::string nName
                                             , std::string nRole ):
                                         Character ( nRole, 0 ), _name ( nName )
{ }


/**
 * Copy constructor
 * @param orig Character whose data is copied into the new character
 * @post The attributes of the new character will have an exact copy of the
 *       values from the original one
 */
ControllableCharacter::ControllableCharacter ( const ControllableCharacter& orig ):
                                        Character ( orig ), _name ( orig._name )
{ }


/**
 * Destructor
 */
ControllableCharacter::~ControllableCharacter ( )
{ }


/**
 * Queries the character for its name
 * @return The name of the character
 */
std::string ControllableCharacter::getName ()
{
   return _name;
}


/**
 * Changes the name of the character
 * @param nName New name of the character
 * @pre No check is performed on the value of nName
 * @post The name of the character changes to the value that is passed as
 *       parameter
 * @return A reference to the character, to allow chained method calls
 */
ControllableCharacter& ControllableCharacter::setName ( std::string& nName )
{
   _name = nName;

   return *this;
}


/**
 * Assignment operator
 * @param other Character whose attributes are copied
 * @post The attributes of the character will store an exact copy of the
 *       attributes of the parameter
 * @return A reference to the character, to allow chained assignments
 */
ControllableCharacter& ControllableCharacter::operator= ( ControllableCharacter& other )
{
   if ( this != &other )
   {
      Character::operator= ( other );

      _name = other._name;
   }

   return *this;
}


void ControllableCharacter::setRole ( std::string nRole )
{
   _role = nRole + " - CC";
}