/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VGException.cpp
 * Author: algarcia
 *
 * Created on 28 de abril de 2021, 9:02
 */

#include "VGException.h"

VGException::VGException ( std::string msg ): std::runtime_error ( msg )
{ }

VGException::VGException ( const VGException& orig ): std::runtime_error ( orig )
{ }

