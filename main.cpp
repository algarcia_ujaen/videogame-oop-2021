/**
 * @file main.cpp
 * @author algarcia
 *
 * @date 10 de febrero de 2021, 9:21
 *
 */

#include <cstdlib>
#include <iostream>
#include <algorithm>

#include "VideoGame.h"
#include "Player.h"
#include "AICharacter.h"
#include "ControllableCharacter.h"
#include "VGException.h"

template<typename T>
void assing ( T& one, T& another )
{
   one = another;
}

/*
 *
 */
int main ( int argc, char** argv )
{  VideoGame* v = nullptr;
   VideoGame v1 ( "Mortal Kombat", "fight", "1.0" );

   try
   {
      v = new VideoGame ( "Angry birds", "strategy", "v1.0" );
      std::cout << "The videogame was successfully created. Its memory address"
                << " is: " << v << std::endl;
   }
   catch ( VGException& e )
   {
      std::cout << "Exception caught! -> " << e.what () << std::endl;
   }
   catch ( std::bad_alloc& e )
   {
      std::cout << "Exception in new! -> " << e.what () << std::endl;
   }
   catch ( std::exception& e )
   {
      std::cout << "Generic exception caught! -> " << e.what () << std::endl;
   }

   VideoGame v2 ( "Tetris", "Arcade", "2.2" );
   VideoGame v3 ( "Tetris", "Arcade", "3.0" );
   v1.setLaunchDate ( 20000101 );
   v2.setLaunchDate ( 20020302 );
   v3.setLaunchDate ( 20040305 );

   if ( v2 == v3 )
   {
      std::cout << "Videogame " << v2.getTitle () << " is equal to videogame "
                << v3.getTitle () << std::endl;
   }
   else
   {
      std::cout << "Videogame " << v2.getTitle () << " and "
                << v3.getTitle () << " are different" << std::endl;
   }

//   do
//   {  std::cout << "Launch date? ";
//      int ld = 0;
//      std::cin >> ld;
//
//      try
//      {
//         if ( std::cin.fail () )   // If cin could not convert the input into an int
//         {  std::cin.clear ();   // Returns cin to "normal" state
//            // Remove all the characters from cin, i.e. leaves cin empty
//            std::cin.ignore ( std::numeric_limits<std::streamsize>::max (), '\n' );
//            throw std::string ( "You have not entered an integer number" );
//         }
//
//         v->setLaunchDate ( ld );
//      }
//      catch ( std::string& e )
//      {  std::cout << "Exception caught! : " << e << std::endl;
//      }
//   }
//   while ( v->getLaunchDate () == 0 );

   // Calling the copy constructor at the same time the object is created
//   VideoGame v2 = *v;
//   VideoGame v3 ( v2 );
//
//   // Calling methods on the objects
//   v2.setLaunchDate ( 1234 );
//   v->setLaunchDate ( 234 );
//   v3.setLaunchDate ( 987 );

   // For every "new" there has to be a "delete" somewhere in the code before
   // the end of the application
   delete v;
   v = nullptr;

   Player p1;
   p1.setGame ( &v1 );
   p1.setGame ( v2 );

   // Adding characters to the video game
   AICharacter* aic = new AICharacter ( "orc", 10 );
   ControllableCharacter* cc = new ControllableCharacter ( "Gandalf", "wizard" );
   v1.addCharacter ( *aic );
   v1.addCharacter ( *cc );

   VideoGame v4 ( v1 );

   v1.changeCharacterRoles ();

   // Changing the life points of a character
   v1.getCharacter (2)->setLifePoints ( 10 );

   // Deleting a character
   v1.deleteCharacter ( 2 );

   // Assignment. WATCH OUT! THE ASSIGNMENT OPERATOR IS NOT FINISHED
   v2 = v1;

   std::cout << "v1 has " << v1.getNumberOfCharacters () << " characters"
             << std::endl;
   std::cout << "v2 has " << v2.getNumberOfCharacters () << " characters"
             << std::endl;

   // The destructor of the objects in the stack is automatically called.
   // The character in v1 will be deleted by the VideoGame destructor

//   Character c1, c2;
   Player p2;

//   assing ( c1, c2 );
//   assing ( p1, p2 );
   assing ( v1, v2 );


   return 0;
}

