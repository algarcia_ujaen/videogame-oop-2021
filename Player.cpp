/**
 * @file Player.cpp
 * @author algarcia
 *
 * @date 10 de marzo de 2021, 9:25
 *
 * @brief Implementation of the class Player methods
 */

#include "Player.h"

/**
 * Copy constructor
 * @param orig Player whose data is copied
 * @post The attributes of the new Player will store an exact copy of the values
 *       from the original one
 */
Player::Player ( const Player& orig ): _realName ( orig._realName )
                                     , _alias ( orig._alias )
                                     , _score ( orig._score )
                                     , _game ( orig._game )
{ }


/**
 * Destructor
 */
Player::~Player ( )
{
   _game = nullptr;
}


/**
 * Changes the score of the player
 * @param newScore New value for the player score
 * @pre No check is performed on the score value
 * @post The score of the player is updated to the value received as parameter
 */
void Player::setScore ( int newScore )
{
   this->_score = newScore;
}


/**
 * Queries the player for their score
 * @return The score of the player
 */
int Player::getScore ( ) const
{
   return _score;
}


/**
 * Changes the nickname of the player
 * @param newAlias New nickname of the player
 * @pre No check is performed on the parameter value
 * @post The player nickname is equal to the parameter value
 */
void Player::setAlias ( std::string newAlias )
{
   this->_alias = newAlias;
}


/**
 * Queries the player for their nickname
 * @return The nickname of the player
 */
std::string Player::getAlias ( ) const
{
   return _alias;
}


/**
 * Changes the real name of the player
 * @param newRealName New real name
 * @pre No check is performed on the parameter value
 * @post The real name of the player is changed to the value of the parameter
 */
void Player::setRealName ( std::string newRealName )
{
   this->_realName = newRealName;
}


/**
 * Queries the player for their real name
 * @return The real name of the player
 */
std::string Player::getRealName ( ) const
{
   return _realName;
}


/**
 * Assigns a video game to the player
 * @param newGame Pointer to the video game the player starts to play with
 * @pre No check is performed on the parameter value
 * @post The game for the player changes according to the parameter
 */
void Player::setGame ( VideoGame* newGame )
{
   this->_game = newGame;
}


/**
 * Queries the player for the game they are playing
 * @return A pointer to the game they are playing, or nullptr if they are not
 *         playing any game
 */
VideoGame* Player::getGame ( ) const
{
   return _game;
}


/**
 * Assigns a video game to the player
 * @param newGame Video game the player starts to play with
 * @pre No check is performed on the parameter value
 * @post The game for the player changes according to the parameter
 */
void Player::setGame (VideoGame& newGame)
{
   this->_game = &newGame;
}

//Player& Player::operator = ( const Player& right )
//{
//   // Check for self-assignment!
//   if ( this == &right ) // Same object?
//      return *this; // Yes, so skip assignment, and just return *this.
//
//   _realName = right._realName;
//   _alias = right._alias;
//   _game = right._game;
//   _score = right._score;
//
//   return *this;
//}