/**
 * @file VideoGame.cpp
 * @author algarcia
 *
 * @date 10 de febrero de 2021, 9:23
 *
 * @brief Implementation of the methods of the VideoGame class
 */

#include "VideoGame.h"
#include "VGException.h"
#include "AICharacter.h"
#include "ControllableCharacter.h"
#include <iostream>

/**
 * @brief Default constructor
 * Initializes the attributes of a game without characters
 */
VideoGame::VideoGame ( ): _characters ( nullptr )
{}


/**
 * Parameterized constructor
 * @param nTitle Title of the new game
 * @param nGenre Genre of the new game
 * @post The values are assigned to the attributes. The game is created without
 *       characters
 * @throw std::string If the version is "v1.0"
 */
VideoGame::VideoGame ( std::string nTitle
                     , std::string nGenre
                     , std::string version ) try : _title ( nTitle ), _genre ( nGenre )
                                                 , _characters ( nullptr )
{
   // If the "result" of the code from the constructor implementation is an
   // exception, the object is not created
   // This code is just to prove this point
//   try   // Uncomment this try-catch to allow object creation
//   {
      if ( version == "v1.0" )
      {
         throw VGException ( "Exception thrown" );
      }
//   }
//   catch ( std::string& e )
//   {
      // However, this catch prevents the object from not being created, as the
      // exception is caught and no further exception is thrown
//      std::cerr << "Captured from inside the constructor->" << e << std::endl;
//   }

   _version = new std::string ( version );
}
catch ( std::string& e )
{
   // In this case, although the exception is caught, it is also thrown to the
   // caller code, and the object is not created
   std::cout << "Captured from the outer try-catch -> " << e << std::endl;
}


/**
 * Copy constructor
 * @param orig Video game whose values are copied
 * @post The new game will store an exact copy of all the attributes, including
 *       the characters
 */
VideoGame::VideoGame ( VideoGame& orig ): _title ( orig._title )
                                              , _genre ( orig._genre )
                                              , _launchDate ( orig._launchDate )
                                              , _characters ( nullptr )
{
   _version = new std::string ( *orig._version );

//   for ( int i = 0; i < orig._characters.getNumElements (); i++ )
//   {
//      Character* auxPtr = new Character ( *orig._characters.getPos ( i + 1 ) );
//      _characters.addElement ( auxPtr );
//   }
   int limit = orig._characters.getNumElements ();
   for ( int i = 0; i < limit; i++ )
   {
      Character* auxPtr = orig._characters.getPos ( i + 1 );

      AICharacter* ptrAIC = dynamic_cast<AICharacter*> ( auxPtr );
      if ( ptrAIC != nullptr )
      {
         AICharacter* newElement = new AICharacter ( *ptrAIC );
         _characters.addElement ( newElement );
      }
      else
      {
         ControllableCharacter* ptrCC = dynamic_cast<ControllableCharacter*> ( auxPtr );
         if ( ptrCC != nullptr )
         {
            ControllableCharacter* newElement = new ControllableCharacter ( *ptrCC );
            _characters.addElement ( newElement );
         }
         else
         {
            throw std::runtime_error ( "Unknown character" );
         }
      }
   }
}


/**
 * Destructor
 */
VideoGame::~VideoGame ( )
{
   delete _version;
   _version = nullptr;

   int nElem = _characters.getNumElements ();

   for ( int i = 0; i < nElem; i++ )
   {
      Character* aux = _characters.takeOut ( 1 );
      delete aux;
   }
}


/**
 * Assigns a new launch date to the videogame
 * @param launchDate New date. It has to have format YYYYMMDD
 *                   (for example: 20210217)
 * @throw std::string If the date is less than 0
 * @post The game launch date is changed to the new value
 */
void VideoGame::setLaunchDate ( int launchDate )
{
   if ( launchDate < 0 )
   {  throw std::invalid_argument ( "VideoGame::setLaunchDate: wrong value" );
   }

   this->_launchDate = launchDate;
}


/**
 * Queries the game for its launch date
 * @return The launch date of the game in format YYYYMMDD (for example: 20200217)
 */
int VideoGame::getLaunchDate ( ) const
{
   return _launchDate;
}


/**
 * Changes the genre of the game
 * @param genre Describes the genre of the game
 * @pre No check is performed on the parameter value
 * @post The genre of the game is changed to the new value
 */
void VideoGame::setGenre ( std::string genre )
{
   this->_genre = genre;
}


/**
 * Queries the game for its genre
 * @return A string identifying the video game genre
 */
std::string VideoGame::getGenre ( ) const
{
   return _genre;
}


/**
 * Changes the title of the videogame
 * @param title The new title of the videogame
 * @pre No check is performed on the parameter value
 * @post The title of the video game is changed to the new value
 */
void VideoGame::setTitle ( std::string title )
{
   this->_title = title;
}


/**
 * Queries the videogame for its title
 * @return A string with the title of the videogame
 */
std::string VideoGame::getTitle ( ) const
{
   return _title;
}


/**
 * Comparison operator.
 *
 * The comparison is based EXCLUSIVELY on the title and the launch date. The
 * other attributes are not taken into account for the comparison
 * @param other The video game to be compared with
 * @retval true If the video games are considered equal
 * @retval false If the video games are considered not equal
 */
bool VideoGame::operator == ( const VideoGame& other )
{
   bool retValue = true;

   if ( this != &other )
   {
      if ( this->_title != other._title )
      {
         retValue = false;
      }

      if ( this->_launchDate != other._launchDate )
      {
         retValue = false;
      }

      // TODO: implement the != operator in the Container template
      if ( this->_characters != other._characters )
      {
         retValue = false;
      }
   }

   // TODO: for a full comparison, compare the characters one by one

   return retValue;
}


/**
 * Assignment operator
 * @param other Game from which the data is copied
 * @return A reference to the video game, in order to allow chained assignments
 * @post The attributes of the video game will store exact copies of all the
 *       information from the original one
 * @note A copy of the version attribute is created, and the old version
 *       attribute is deleted
 * @todo Delete the characters and create copies for the new ones
 */
VideoGame& VideoGame::operator= ( VideoGame& other )
{
   if ( this != &other )
   {
      this->_genre = other._genre;
      this->_launchDate = other._launchDate;
      this->_title = other._title;

      if ( this->_version != nullptr )
      {
         delete this->_version;
      }

      this->_version = new std::string ( *(other._version) );

      // 1st->delete the characters in the "this" object
      int nElems = _characters.getNumElements ();
      for ( int i = 0; i < _characters.getNumElements (); i++ )
      {
         Character* auxC = _characters.takeOut ( 1 );
         delete auxC;
      }

      // Note: The container itself is setting to nullptr the positions not in use

      // 2nd->create new characters copying the ones in the "other" object
      nElems = other._characters.getNumElements ();
//      for ( int i = 0; i < nElems; i++ )
//      {
//         Character* auxC = new Character ( *other._characters.getPos ( i+1 ) );
//         _characters.addElement ( auxC );
//      }
   }

   return *this;
}


/**
 * Adds a new character to the video game
 * @param nC New character
 * @pre No check is performed on the parameter value
 * @post The video game has a new character, if possible
 * @return A reference to the game, in order to allow chained method calls
 * @throw std::length_error If there is no room for another character in the
 *                          game
 */
//VideoGame& VideoGame::addCharacter ( Character& nC )
//{
//   // std::length_error now is thrown by Container::addElement
//   Character* auxC = new Character ( nC );
//   _characters.addElement ( auxC );
//   auxC = nullptr;
//   return *this;
//}


/**
 * Adds a new character to the video game
 * @param nC New character
 * @pre No check is performed on the parameter value
 * @post The video game has a new character, if possible
 * @return A reference to the game, in order to allow chained method calls
 * @throw std::length_error If there is no room for another character in the
 *                          game
 */
VideoGame& VideoGame::addCharacter ( AICharacter& nC )
{
   // std::length_error now is thrown by Container::addElement
   Character* auxC = new AICharacter ( nC );
   _characters.addElement ( auxC );
   auxC = nullptr;
   return *this;
}


/**
 * Adds a new character to the video game
 * @param nC New character
 * @pre No check is performed on the parameter value
 * @post The video game has a new character, if possible
 * @return A reference to the game, in order to allow chained method calls
 * @throw std::length_error If there is no room for another character in the
 *                          game
 */
VideoGame& VideoGame::addCharacter ( ControllableCharacter& nC )
{
   // std::length_error now is thrown by Container::addElement
   Character* auxC = new ControllableCharacter ( nC );
   _characters.addElement ( auxC );
   auxC = nullptr;
   return *this;
}


/**
 * Queries the video game for a character
 * @param which Index of the character. Value has to be in the range
 *              [1..number of characters]
 * @return The memory address of the character, or nullptr if there is no
 *         character identified with the index
 * @throw std::length_error If the parameter value is out of the bounds of the
 *        aforementioned range
 */
Character* VideoGame::getCharacter ( int which )
{
   // std::length_error now is thrown by Container::getPos
   return _characters.getPos ( which );
}


/**
 * Removes a character from the video game
 * @param which Index of the character. Value has to be in the range
 *              [1..number of characters]
 * @post The game has one character less
 * @throw std::length_error If the parameter value is out of the bounds of the
 *        aforementioned range
 * @todo Compact the data of the vector after deletion
 */
void VideoGame::deleteCharacter ( int which )
{
   Character* auxC = _characters.takeOut ( which );
   delete auxC;
   auxC = nullptr;
}


/**
 * Queries the video game for the amount of characters it has
 * @return The number of characters the game has
 */
int VideoGame::getNumberOfCharacters ( )
{
   return _characters.getNumElements ();
}

void VideoGame::changeCharacterRoles ( )
{
   for ( int i = 0; i < _characters.getNumElements (); i++ )
   {
      _characters.getPos ( i+1 )->setRole ( "asddf" );
   }
}
